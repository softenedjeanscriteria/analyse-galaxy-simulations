import os
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
np.seterr(divide = 'ignore')

import matplotlib.pylab as pylab
params = {'legend.fontsize': 'large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'figure.titlesize':'x-large',
         'legend.title_fontsize':'large',
         'legend.fontsize':'large',
         'xtick.labelsize':'large',
         'ytick.labelsize':'large'}
pylab.rcParams.update(params)

import sys
sys.path.insert(0, "helpers/")

from equations import density_critical_hmin

from simulations_details import get_simulation_time_Myr
from simulations_details import get_simulation_details_all

from select_simulations_for_figures import Fig8_runnames as runnames
from select_simulations_for_figures import Fig8_filebase_runs as filebase_runs
from select_simulations_for_figures import Fig8_filebase_reruns as filebase_reruns

from simulation_path import snapshotnumber

from simulation_plotparameter import lognH_min
from simulation_plotparameter import lognH_max
from simulation_plotparameter import dlognH_ticks
from simulation_plotparameter import logT_min
from simulation_plotparameter import logT_max
from simulation_plotparameter import massfrac_min
from simulation_plotparameter import massfrac_max
from simulation_plotparameter import nbins
from simulation_plotparameter import colors
from simulation_plotparameter import lognH_2Darr
from simulation_plotparameter import logT_2Darr
from simulation_plotparameter import nH_2Darr
from simulation_plotparameter import T_2Darr
from simulation_plotparameter import color_zone
from simulation_plotparameter import eta_res

from phasespace_plot import cumulative_mass_fraction
from phasespace_plot import make_individual_phasespace_plot
from phasespace_plot import add_zone_of_avoidance
from phasespace_plot import add_Jeans_mass_contours

snapshotnumber_rerun = 0

############################################################################
# Set output filename
############################################################################
outputname = "Fig8_phasespace_comparison.png"


# #################################################################
# combines phase space plots and cumulative mass fraction plots
# into one figure with:
# row 1 (columns 0-2): phase-space plots for original snapshots for runs 
#                      with 3 different minimum smoothing lengths
# row 2 (columns 0-2): phase-space plots for real densities
# final column: cumulative mass fractions for densities above x-axis value
# #################################################################
def make_plot():
    mB_runs, eps_runs, h_min_runs, h_min_ratio_runs, XH_runs, esf_runs, \
    mB_reruns, eps_reruns, h_min_reruns, h_min_ratio_reruns, XH_reruns, esf_reruns = \
                                     get_simulation_details_all(filebase_runs, filebase_reruns, \
                                                           snapshotnumber)

    #get simulation time
    time_Myr = get_simulation_time_Myr(filebase_runs[0] + '%4.4i'%(snapshotnumber) + '.hdf5')

    fig = plt.figure(figsize = (12.3,5.0))
    fig.subplots_adjust(bottom = 0.12 , left = 0.10, right = 0.97, top = 0.85)

    titlestring  = "log m$_{\mathrm{B}}$ [M$_{\odot}$] = %.1f, "%(np.log10(mB_runs[0]))
    titlestring += "$\epsilon$ = %i pc, "%(round(eps_runs[0]))
    titlestring += "e$_{\mathrm{sf}}$ = %.2f"%(esf_runs[0]*100.)
    titlestring += " per cent, "
    titlestring += "t = %.2f Gyr"%(time_Myr/1000.)

    fig.suptitle(titlestring)
    gs = gridspec.GridSpec(2,5, width_ratios = [1.,1.,1.,0.32,1.], wspace = 0., hspace = 0.)

    # add phasespace 2D histograms for original snapshot  
    for iplot in range(len(filebase_runs)):
        snapfilename = filebase_runs[iplot] + '%4.4i'%(snapshotnumber) + '.hdf5'
        ax = plt.subplot(gs[iplot])
        ax.set_title("h$_{\mathrm{min}}$ = %.2f pc"%(h_min_runs[iplot]))
        ax.tick_params(axis = 'x', labelbottom = False, direction = 'inout')
        mass_map = make_individual_phasespace_plot(ax, snapfilename)
        add_zone_of_avoidance(ax, mB_runs[iplot], eps_runs[iplot], \
                                  h_min_runs[iplot], XH_runs[iplot], mass_map)

        add_Jeans_mass_contours(ax, mB_runs[iplot], eps_runs[iplot], \
                                  h_min_runs[iplot], XH_runs[iplot])
        if iplot == 0:
            ax.set_ylabel('log T [K]')
            ax.text(-0.34, 0.5, "SPH estimated\ngas densities", ha = 'center', va = 'center', transform = ax.transAxes, fontsize = 'x-large', rotation = 90)
            ax.text(0.98, 0.5, "log M$_{\mathrm{C}}$", ha = 'right', va = 'baseline', transform = ax.transAxes, fontsize = 'x-large')
        else:
            ax.tick_params(axis = 'y', labelleft = False, direction = 'inout')

        if iplot < 2:
            ax.text(0.97, 0.95, '(C)', transform = ax.transAxes, ha = 'right', va = 'top', fontsize = 'x-large', zorder = 2000)
        else:
            ax.text(0.97, 0.05, '(C)', transform = ax.transAxes, ha = 'right', va = 'baseline', fontsize = 'x-large', zorder = 2000)
        
    # add phasespace 2D histograms for re-calculated (hmin -> 0) snapshot
    for iplot in range(len(filebase_reruns)):
        snapfilename = filebase_reruns[iplot] + '%4.4i'%(snapshotnumber_rerun) + '.hdf5'
        ax = plt.subplot(gs[iplot+5])
        ax.set_xlabel('log n$_{\mathrm{H}}$ [cm$^{-3}$]')
        mass_map = make_individual_phasespace_plot(ax, snapfilename)
        add_zone_of_avoidance(ax, mB_runs[iplot], eps_runs[iplot], \
                                  h_min_runs[iplot], XH_runs[iplot], mass_map)

        add_Jeans_mass_contours(ax, mB_runs[iplot], eps_runs[iplot], \
                                  h_min_runs[iplot], XH_runs[iplot])

        if iplot == 0:
            ax.set_ylabel('log T [K]')
            ax.text(-0.34, 0.5, "Re-calculated\ngas densities", ha = 'center', va = 'center', transform = ax.transAxes, fontsize = 'x-large', rotation = 90)
        else:
            ax.tick_params(axis = 'y', labelleft = False, direction = 'inout')

        if iplot < 2:
            ax.text(0.97, 0.95, '(C)', transform = ax.transAxes, ha = 'right', va = 'top', fontsize = 'x-large', zorder = 2000)
        else:
            ax.text(0.97, 0.05, '(C)', transform = ax.transAxes, ha = 'right', va = 'baseline', fontsize = 'x-large', zorder = 2000)
        
    # mass fractions for >n 
    ax_hi = plt.subplot(gs[4])
    ax_hi.set_xlim(lognH_min, lognH_max)
    ax_hi.xaxis.set_ticks(np.arange(lognH_min, lognH_max+dlognH_ticks, dlognH_ticks))
    ax_hi.set_yscale('log')
    ax_hi.set_ylim(massfrac_min, massfrac_max)
    ax_hi.set_ylabel(r'M$_{ (> {\mathrm{n}}_{\mathrm{H}}) }$ / M$_{\mathrm{tot}}$')
    ax_hi.tick_params(axis = 'x', labelbottom = False, direction = 'inout')
    ax_lo = plt.subplot(gs[9])
    ax_lo.set_xlim(lognH_min, lognH_max)
    ax_lo.xaxis.set_ticks(np.arange(lognH_min, lognH_max+dlognH_ticks, dlognH_ticks))
    ax_lo.set_yscale('log')
    ax_lo.set_ylim(massfrac_min, massfrac_max)
    ax_lo.set_ylabel(r'M$_{ (> {\mathrm{n}}_{\mathrm{H}}) }$ / M$_{\mathrm{tot}}$')
    ax_lo.set_xlabel('log n$_{\mathrm{H}}$ [cm$^{-3}$]')

    for iplot in range(len(filebase_runs)):
        snapfilename = filebase_runs[iplot] + '%4.4i'%(snapshotnumber) + '.hdf5'
        lognH, massfraction = cumulative_mass_fraction(snapfilename)       
        ax_hi.plot(lognH, massfraction, color = colors[iplot], linestyle = 'solid', label = "%.2f"%(h_min_runs[iplot]))
        nHcrit = density_critical_hmin(mB_runs[iplot], h_min_runs[iplot])
        ax_hi.axvline(np.log10(nHcrit), ymin = 0.9, ymax = 1.0, color = colors[iplot], linestyle = 'dotted', lw = 2)
        ax_lo.axvline(np.log10(nHcrit), ymin = 0.9, ymax = 1.0, color = colors[iplot], linestyle = 'dotted', lw = 2)

    for iplot in range(len(filebase_reruns)):
        snapfilename = filebase_reruns[iplot] + '%4.4i'%(snapshotnumber_rerun) + '.hdf5'
        lognH, massfraction = cumulative_mass_fraction(snapfilename)
        ax_hi.plot(lognH, massfraction, color = colors[iplot], linestyle = 'dashed', lw = 1.0, alpha = 0.5)    
        ax_lo.plot(lognH, massfraction, color = colors[iplot], linestyle = 'dashed')    

    ax_hi.legend(loc = "lower left", title = 'h$_{\mathrm{min}}$ [pc]')

    fig.savefig(outputname, dpi = 150)
    plt.close()
    print ("Figure saved: "+outputname)


make_plot()
