import os
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import patches

import numpy as np
np.seterr(divide = 'ignore')

from swiftsimio import load

import matplotlib.pylab as pylab
params = {'legend.fontsize': 'large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'figure.titlesize':'x-large',
         'legend.title_fontsize':'large',
         'legend.fontsize':'large',
         'xtick.labelsize':'large',
         'ytick.labelsize':'large'}
pylab.rcParams.update(params)

import sys
sys.path.insert(0, "helpers/")

from plotannotation import add_arrows

from stellar_surface_density_plot import add_stellar_surface_density
from stellar_surface_density_plot import add_fof_groups

from simulations_details import get_simulation_time_Myr
from simulations_details import get_simulation_details_all

from select_simulations_for_figures import Fig9_runnames as runnames
from select_simulations_for_figures import Fig9_filebase_runs as filebase_runs
from select_simulations_for_figures import Fig9_filebase_reruns as filebase_reruns
from select_simulations_for_figures import Fig9_filebase_fofruns as filebase_fofruns

from simulation_path import snapshotnumber


snapshotnumber_rerun = 0
############################################################################
# Set output filename
############################################################################
outputname = "Fig7_gallery_stars.png"

############################################################################
# Some extra plotting settings 
############################################################################

axis_xsize = 0.40
axis_ysize = axis_xsize
axis_dy    = 0.015
axis_dx_left   = 0.02
axis_dx        = 0.2
axis_dy_bottom = 0.02
axis_overlap   = 0.1
axis_overlap_x = 0.02

# #################################################################
# Creates 5 stellar mass density plots 
# of the 5 runs identified in helpers/Fig7_runs_to_plot.py
# Labels need to be updated if different runs are selected
# #################################################################

def make_stars_gallery_plot(outputname):
    mB_runs, eps_runs, h_min_runs, h_min_ratio_runs, XH_runs, esf_runs, \
    mB_reruns, eps_reruns, h_min_reruns, h_min_ratio_reruns, XH_reruns, esf_reruns = \
                                     get_simulation_details_all(filebase_runs, filebase_reruns, \
                                                           snapshotnumber)

    #get simulation time
    time_Myr = get_simulation_time_Myr(filebase_runs[0] + '%4.4i'%(snapshotnumber) + '.hdf5')

    fig = plt.figure(figsize = (7.5*0.9,7*0.9))
    axframe = fig.add_axes([0,0,1,1])
    for axis in ['top','bottom','left','right']:
        axframe.spines[axis].set_linewidth(10)
        axframe.spines[axis].set_color("black")
        axframe.spines[axis].set_zorder(0)

    ax = []

    ####################################################### 
    # Setup axes
    ####################################################### 
    # 0: top left panel
    ax.append(fig.add_axes([axis_dx_left, \
                            1. - axis_dy_bottom - axis_ysize, \
                            axis_xsize, \
                            axis_ysize]))
    # 1: top right panel
    ax.append(fig.add_axes([1. - axis_dx_left - axis_xsize , \
                            1. - axis_dy_bottom - axis_ysize, \
                            axis_xsize, \
                            axis_ysize]))
    # 2: middle panel
    ax.append(fig.add_axes([0.5 - 0.5 * axis_xsize, \
                            0.5 - 0.5 * axis_ysize, \
                            axis_xsize, \
                            axis_ysize]))
    # 3: bottom left panel
    ax.append(fig.add_axes([axis_dx_left, \
                            axis_dy_bottom, \
                            axis_xsize, \
                            axis_ysize]))
    # 4: bottom right panel
    ax.append(fig.add_axes([1. - axis_dx_left - axis_xsize , \
                            axis_dy_bottom, \
                            axis_xsize, \
                            axis_ysize]))

    for iplot in range(len(filebase_reruns)):
        snapfilename = filebase_reruns[iplot] + '%4.4i'%(snapshotnumber_rerun) + '.hdf5'
        mass_map_plot = add_stellar_surface_density(ax[iplot], snapfilename)

        foffilename  = filebase_fofruns[iplot]
        add_fof_groups(ax[iplot], foffilename)
   
    ax[2].text(1.2, 0.5, "Increase m$_{\mathrm{B}}$\n (x 8)", transform = ax[2].transAxes, fontsize = 'x-large', va = 'center', ha = 'left')
    ax[2].text(0.5, 1.35, "Increase $\epsilon$\n (x 2)", transform = ax[2].transAxes, fontsize = 'x-large', va = 'bottom', ha = 'center')
    ax[2].text(0.5,-0.35, "Decrease h$_{\mathrm{min}}$\n (x 0.1)", transform = ax[2].transAxes, fontsize = 'x-large', va = 'top', ha = 'center')
    ax[2].text(-0.2,0.5, "Increase e$_{\mathrm{sf}}$\n (x 3)", transform = ax[2].transAxes, fontsize = 'x-large', va = 'center', ha = 'right')

    add_arrows(fig, ax)

    plt.savefig(outputname, dpi = 150)
    plt.close()
    print ("Figure saved: "+outputname)
    
    return

make_stars_gallery_plot(outputname)
