import numpy as np
import glob
import matplotlib.pyplot as plt
from scipy.stats import binned_statistic
import matplotlib.gridspec as gridspec
import re
import os

import matplotlib.pylab as pylab
params = {'legend.fontsize': 'large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'large',
         'ytick.labelsize':'large'}
pylab.rcParams.update(params)

import sys
sys.path.insert(0, "helpers/")
from select_simulations_for_figures import Fig11_runnames as runnames
from select_simulations_for_figures import Fig11_filebase_runs as filebase_runs
from select_simulations_for_figures import Fig11_filebase_reruns as filebase_reruns

############################################################################
# Set output filename
############################################################################
outputname = "Fig11_sfr.png"


fig = plt.figure(figsize = (6/1.1,4/1.1))
fig.subplots_adjust(bottom = 0.17, right = 0.88)
fig.suptitle('Simulations with m$_{\mathrm{B}}$ = 10$^5$ M$_{\odot}$, e$_{\mathrm{sf}}$ = 0.32 per cent')
gs = gridspec.GridSpec(1, 2, width_ratios = [1,0.1], wspace = 0.05)

ax  = plt.subplot(gs[0])

for filebase in filebase_runs:
    folder, _ = os.path.split(filebase)
    filename = folder+'/SFR.txt'
    time, SFR = np.loadtxt(filename, usecols=(1, 7), unpack = True)
    time_Myr = 9.778131e+02 * time
    SFR_Msol_per_yr = 1.022726e+01 * SFR
    
    SFRmean, timebins_edges, binnumber = binned_statistic(time_Myr, SFR_Msol_per_yr, statistic = 'mean', bins = 120)

    bin_width = (timebins_edges[1] - timebins_edges[0])
    timebins_centers = timebins_edges[1:] - bin_width/2

    leg = ''
    if 'soft0250pc' in folder:
        ls = 'dashed'
        #leg += '$\epsilon$ = 250 pc, '
    elif 'soft0500pc' in folder:
        ls = 'solid'
        #leg += '$\epsilon$ = 500 pc, '

    if 'hmin077' in folder:
        c = '#CC0066'
        leg += 'h$_{\mathrm{min}}$ = 77.5 pc'
    elif 'hmin024' in folder:
        c = '#0066CC'
        leg += 'h$_{\mathrm{min}}$ = 24.5 pc'
    else:
        c = '#00CCCC'
        leg += 'h$_{\mathrm{min}}$ = 7.75 pc'

    ax.plot(timebins_centers, SFRmean, label = leg, ls = ls, c = c)

h, l = ax.get_legend_handles_labels()
leg_hmin = ax.legend(h[5:2:-1], l[5:2:-1], loc = 'upper right')
leg_eps  = ax.legend([h[0], h[3]], ['$\epsilon$ = 250 pc', '$\epsilon$ = 500 pc'], loc = 'center right')
ax.add_artist(leg_hmin)
ax.add_artist(leg_eps)

ax.set_xlabel('t [Myr]')
ax.set_ylabel('SFR$_{\mathrm{10Myr}}$ [M$_{\odot}$ yr$^{-1}$]')
#######################################################
ax = plt.subplot(gs[1])
ax.set_xticks([])
ax.set_xticklabels([])
ax.yaxis.set_label_position("right")
ax.yaxis.tick_right()
ax.set_ylabel('Relative computing time')
Mgasmin = 10099679000.0

for filebase in filebase_runs:
    folder, _ = os.path.split(filebase)
    filename_wc = folder+'/output-*.out'
    filename = glob.glob(filename_wc)[0]

    with open(filename, 'r') as f:
        last_line = f.readlines()[-1]
    
    wallclocktime = float(re.findall(r'[\d]*[.][\d]+', last_line)[0])

    if 'soft0250pc' in folder:
        ls = 'dashed'
        m  = 'o'
    elif 'soft0500pc' in folder:
        ls = 'solid'
        m = 's'

    if 'hmin077' in folder:
        c = '#CC0066'
    elif 'hmin024' in folder:
        c = '#0066CC'
    else:
        c = '#00CCCC'

    ax.plot([0., 1.], [wallclocktime/100096.1, wallclocktime/100096.1], ls = ls, c = c) 

fig.savefig(outputname, dpi = 150)
plt.close()
print ("Figure saved: "+outputname)

    


