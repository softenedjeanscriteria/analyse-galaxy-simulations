import os
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
np.seterr(divide = 'ignore') 

from swiftsimio import load

import matplotlib.pylab as pylab
params = {'legend.fontsize': 'large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'figure.titlesize':'x-large',
         'legend.title_fontsize':'large',
         'legend.fontsize':'large',
         'xtick.labelsize':'large',
         'ytick.labelsize':'large'}
pylab.rcParams.update(params)

from unyt import proton_mass_cgs as mH
from unyt import cm, K, g
from unyt import msun, pc

from simulation_plotparameter import lognH_min
from simulation_plotparameter import lognH_max
from simulation_plotparameter import dlognH_ticks
from simulation_plotparameter import logT_min
from simulation_plotparameter import logT_max
from simulation_plotparameter import massfrac_min
from simulation_plotparameter import massfrac_max
from simulation_plotparameter import nbins
from simulation_plotparameter import colors
from simulation_plotparameter import lognH_2Darr
from simulation_plotparameter import logT_2Darr
from simulation_plotparameter import nH_2Darr
from simulation_plotparameter import T_2Darr
from simulation_plotparameter import color_zone
from simulation_plotparameter import color_zone_C
from simulation_plotparameter import color_zone_D

from equations import Jeans_length_0
from equations import Jeans_length_W
from equations import Jeans_mass_0
from equations import Jeans_mass_W
from equations import density_critical_hmin
from equations import eta_res
from equations import h_smooth


Nneigh = 50

# #################################################################
# adds zone of induced collapse to axis
# #################################################################
def add_zone_of_induced_collapse(ax, mB, eps, XH):

    softening_scale = 1.5 * eps
    kernel_size = 1.936492 * h_smooth(mB, nH_2Darr, 0.)

    all_zones = np.zeros_like(nH_2Darr)
    all_zones[...] = 1.

    # Zone D:
    all_zones[ (Jeans_length_W(nH_2Darr, T_2Darr, eps) >  softening_scale) & \
               (Jeans_length_W(nH_2Darr, T_2Darr, eps) <= kernel_size) ] = 0.

    ax.contourf(lognH_2Darr, logT_2Darr, all_zones, levels = np.asarray([0., 0.99]), colors = color_zone_D, alpha = 0.8)
    ax.contour (lognH_2Darr, logT_2Darr, all_zones, levels = np.asarray([0.99]), colors = color_zone_D, zorder = 100)    

    return
    

# #################################################################
# adds zone of avoidance to axis
# #################################################################
def add_zone_of_avoidance(ax, mB, eps, h_min, XH, mass_map):
    zone_of_avoidance = np.zeros_like(lognH_2Darr)
    zone_of_avoidance[:,:] = 1.
    zone_of_avoidance[(Jeans_length_W(nH_2Darr, T_2Darr, eps) / h_min < 1.) & \
                      (lognH_2Darr > np.log10(density_critical_hmin(mB, h_min)))] = 0.

    mass_zone = np.sum(mass_map[zone_of_avoidance == 0.])

    ax.contourf(lognH_2Darr, logT_2Darr, zone_of_avoidance, levels = np.asarray([0., 0.99]), colors = color_zone_C, alpha = 0.8)
    ax.contour (lognH_2Darr, logT_2Darr, zone_of_avoidance, levels = np.asarray([0.99]), colors = color_zone_C, zorder = 100)
    if mass_zone > 0.:
        ax.text(0.98, 0.38, "%.2f"%(np.log10(mass_zone)), ha = 'right', va = 'baseline', transform = ax.transAxes, fontsize = 'x-large')

    return

# #################################################################
# adds contours for softened Jeans mass equals 1 and 10 times 
# the baryon particle mass
# #################################################################
def add_Jeans_mass_contours(ax, mB, eps, h_min, XH):
    logMJ_W_2D_over_mB = np.log10(Jeans_mass_W(nH_2Darr, T_2Darr, eps) / (1. * mB))

    ax.contour (lognH_2Darr, logT_2Darr, logMJ_W_2D_over_mB, levels = np.asarray([2.]), colors = 'black', linestyles = 'solid', zorder = 100, linewidths = 1)
    ax.contour (lognH_2Darr, logT_2Darr, logMJ_W_2D_over_mB, levels = np.asarray([1.]), colors = 'black', linestyles = 'dashed', zorder = 100, linewidths = 1)
    ax.contour (lognH_2Darr, logT_2Darr, logMJ_W_2D_over_mB, levels = np.asarray([0.]), colors = 'black', linestyles = 'dotted', zorder = 100, linewidths = 1)

    return

# #################################################################
# plots 2D density temperature histogram into axis
# for one snapshot
# #################################################################
def make_individual_phasespace_plot(ax, filename):
    data = load(filename)
    SPH_hydrogen_fraction = data.gas.element_mass_fractions.hydrogen
    SPH_number_density    = (data.gas.densities / mH * SPH_hydrogen_fraction ).to(cm ** -3)
    SPH_temperatures      = (data.gas.temperatures).to('K')
    SPH_masses            = (data.gas.masses).to('msun')

    SPH_hist, xedge, yedge = np.histogram2d(np.log10(SPH_number_density), np.log10(SPH_temperatures), bins = nbins, \
                         range = [[lognH_min, lognH_max], [logT_min, logT_max]], weights = SPH_masses, density = False)  

    SPH_hist = SPH_hist.T
    logSPH_hist = np.log10(SPH_hist)

    ax.imshow(logSPH_hist, interpolation='none', origin='lower',\
              extent=[xedge[0], xedge[-1], yedge[0], yedge[-1]], \
              aspect='auto', zorder = 10, vmin = 5., vmax = 7., cmap = 'viridis')

    ax.set_xlim(lognH_min, lognH_max)
    ax.set_ylim(logT_min, logT_max)
    ax.xaxis.set_ticks(np.arange(lognH_min, lognH_max, dlognH_ticks))

    return SPH_hist

# #################################################################
# calculates the mass fraction of particles with densities above n
# for various n
# #################################################################
def cumulative_mass_fraction(filename):
    data = load(filename)
    data.gas.masses.convert_to_units('Msun')
    data.gas.densities.convert_to_units(g * cm**-3)
    nH = data.gas.densities / mH * data.gas.element_mass_fractions.hydrogen
    nH.convert_to_units(cm**-3)

    data.gas.masses[np.log10(nH.value) < lognH_min] = 0.    

    histbins_dens = np.logspace(lognH_min, lognH_max, num = nbins, endpoint=True, base=10.0)
    hist, edges = np.histogram(nH, bins = histbins_dens, weights = data.gas.masses.value, density = False)
    cumhist  = np.cumsum(hist) / np.sum(data.gas.masses.value)

    return np.log10(histbins_dens[:-1]), 1. - cumhist

