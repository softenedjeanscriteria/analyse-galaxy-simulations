import os
import sys
from swiftsimio import load
import numpy as np

from simulation_path import simulation_path as folderbase
from simulation_path import snapshotnumber

####################################################
# Returns the full paths and runnames for a simulation with the
# selected values for mB, eps, hmin, and the star
# formation efficiency
####################################################
def get_runfolders(mB, eps, hmin, starforme):
    if len(mB) == len(eps) == len(hmin) == len(starforme):

        runfolders = []
        rerunfolders = []
        foffolders = []
        for irun in range (len(mB)):
            mass = mB[irun]
            soft = eps[irun]
            smooth = hmin[irun] 
            sfe    = starforme[irun]
            runname = "Galaxy" + mass + \
                          "_soft%4.4ipc"%(int(soft*1000)) + \
                          "_hmin%06.2fpc"%(smooth*1000.) + \
                          "_sfe%06.3f"%(sfe)
            runfolders.append( os.path.join(folderbase, mass, runname) )
            rerunfolders.append( os.path.join(folderbase, mass+"_reruns_snap%4.4i"%(snapshotnumber), "Rerun" + runname) )
            foffolders.append( os.path.join(folderbase, mass+"_fofruns_snap%4.4i"%(snapshotnumber), "FOF"+runname)  )

        filebase_runs   = [s + "/output_" for s in runfolders]
        filebase_reruns = [s + "/output_" for s in rerunfolders]
        filebase_fofruns = [s + "/fof_output_0000.hdf5" for s in foffolders]

        runnames = [os.path.split(s)[1] for s in runfolders]

    else:
        print("[get_runfolders] inconsistent simulation parameters")
        sys.exit()

    return runnames, filebase_runs, filebase_reruns, filebase_fofruns

# #################################################################
# returns the snapshot time in Myr
# #################################################################
def get_simulation_time_Myr(filename):
    data = load(filename)
    data.metadata.time.convert_to_units('Myr')
    return data.metadata.time.value


# #################################################################
# returns some basic simulation information from one snapshot
# #################################################################
def get_simulation_details(filename):
    data = load(filename)
    eps         = float(data.metadata.parameters.get('Gravity:max_physical_baryon_softening')) * 1000.
    h_min_ratio = float(data.metadata.parameters.get('SPH:h_min_ratio'))
    h_min       = float(data.metadata.parameters.get('SPH:h_min_ratio')) * 1.55 * eps
    XH_init     = float(data.metadata.parameters.get('SPH:H_mass_fraction'))
    e_sf        = float(data.metadata.parameters.get('EAGLEStarFormation:star_formation_efficiency'))

    Mgas_i = data.metadata.initial_mass_table.gas.to('Msun').value
    if Mgas_i == 0:
        mB = data.gas.masses.to('Msun')
        Mgas_i = (np.sum(mB)/float(len(mB))).value
    mB = Mgas_i

    return mB, eps, h_min, h_min_ratio, XH_init, e_sf

# #################################################################
# returns some basic simulation information from list of runs and reruns
# #################################################################
def get_simulation_details_all(filebase_runs, filebase_reruns, \
                                snapshotnumber):

    snapshotnumber_rerun = 0
    mB_runs  = []
    eps_runs = []
    h_min_runs = []
    h_min_ratio_runs = []
    XH_runs = []
    e_sf_runs = []

    for iplot in range(len(filebase_runs)):
        snapfilename = filebase_runs[iplot] + '%4.4i'%(snapshotnumber) + '.hdf5'
        mB, eps, h_min, h_min_ratio, XH_init, e_sf = get_simulation_details(snapfilename)
        mB_runs.append(mB)
        eps_runs.append(eps)
        h_min_runs.append(h_min)
        h_min_ratio_runs.append(h_min_ratio)
        XH_runs.append(XH_init)
        e_sf_runs.append(e_sf)

    mB_reruns  = []
    eps_reruns = []
    h_min_reruns = []
    h_min_ratio_reruns = []
    XH_reruns = []
    e_sf_reruns = []

    for iplot in range(len(filebase_reruns)):
        snapfilename = filebase_reruns[iplot] + '%4.4i'%(snapshotnumber_rerun) + '.hdf5'
        mB, eps, h_min, h_min_ratio, XH_init, e_sf = get_simulation_details(snapfilename)
        mB_reruns.append(mB)
        eps_reruns.append(eps)
        h_min_reruns.append(h_min)
        h_min_ratio_reruns.append(h_min_ratio)
        XH_reruns.append(XH_init)
        e_sf_reruns.append(e_sf)

    return np.asarray(mB_runs), np.asarray(eps_runs), np.asarray(h_min_runs), \
           np.asarray(h_min_ratio_runs), np.asarray(XH_runs), np.asarray(e_sf_runs),  \
           np.asarray(mB_reruns), np.asarray(eps_reruns), np.asarray(h_min_reruns), \
           np.asarray(h_min_ratio_reruns), np.asarray(XH_reruns), np.asarray(e_sf_reruns)


