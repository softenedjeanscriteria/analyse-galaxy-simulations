import numpy as np
from unyt import msun, pc
from unyt import proton_mass_cgs as mH

XH = 0.74
eta_res = 1.2348

############################################################################
# nH ... gas density in cm-3
# T  ... gas temperature in K
# returns the Newtonian Jeans length in pc
############################################################################
def Jeans_length_0(nH,T):
    return 5.4 * np.power(T, 1./2.) * np.power(nH, -1./2.)

############################################################################
# nH ... gas density in cm-3
# T  ... gas temperature in K
# returns the Newtonian Jeans mass in Msol
############################################################################
def Jeans_mass_0(nH, T):
    return 21.2 * np.power(T, 3./2.) * np.power(nH, -1./2.)

############################################################################
# nH ... gas density in cm-3
# T  ... gas temperature in K
# returns the Plummer softened Jeans mass in Msol
############################################################################
def Jeans_mass_P(nH, T, eps):
    return Jeans_mass_0(nH, T) * np.power(1. + 1.42 * np.power(eps/Jeans_length_0(nH,T), 3./2.), 6./5.)

############################################################################
# nH ... gas density in cm-3
# T  ... gas temperature in K
# returns the softened Jeans length (for Wendland C2) in pc
############################################################################
def Jeans_length_W(nH,T,eps):
    H = 3. * eps
    return Jeans_length_0(nH,T) * np.power(1. + 0.27 * np.power(H / Jeans_length_0(nH,T), 2.), 0.3)

############################################################################
# nH ... gas density in cm-3
# T  ... gas temperature in K
# returns the Wendland C2 softened Jeans mass in Msol
############################################################################
def Jeans_mass_W(nH, T, H):
    return Jeans_mass_0(nH, T) * np.power(1. + 0.27 * np.power(H/Jeans_length_0(nH,T), 2.), 9./10.)


############################################################################
# mB ... particle mass resolution in Msol
# hmin ... minimum smoothing length in pc
# returns the density above which the smoothing length is limited by hmin
############################################################################
def density_critical_hmin(mB, hmin):
    mBsol = mB * msun
    hminpc = hmin *  pc
    nH = XH / mH * np.power(eta_res,3) * mBsol / np.power(hminpc, 3.)
    nH.convert_to_units('cm**-3')
    return nH


############################################################################
# mB ... particle mass resolution in Msol
# nH ... gas density in cm-3
# hmin ... minimum smoothing length in pc
# returns the smoothing length in pc
############################################################################
def h_smooth(mB, nH, hmin):
    h_pc = 82. * np.power(mB / 1.e6, 1./3.) * np.power(nH / 100., -1./3.)
    h_pc = np.maximum(h_pc, hmin)
    return h_pc






