
import numpy as np

lognH_min = -4.
lognH_max =  8.
dlognH_ticks = 2.

logT_min  = 0.8
logT_max  = 4.5

nbins = 128

lognH_arr = np.linspace(lognH_min, lognH_max, nbins)
logT_arr  = np.linspace(logT_min , logT_max, nbins)

lognH_2Darr = np.tile(lognH_arr, (len(logT_arr), 1))
logT_2Darr  = (np.tile(logT_arr, (len(lognH_arr), 1))).T

nH_2Darr = np.power(10., lognH_2Darr)
T_2Darr = np.power(10., logT_2Darr)

massfrac_min = 0.005
massfrac_max = 2.

colors = []
colors.append('#CC0066')
colors.append('grey')
colors.append('#0066CC')

color_zone = '#CC0066'
import matplotlib
norm = matplotlib.colors.Normalize(vmin=0.0, vmax=8.0)
cmap = matplotlib.cm.get_cmap('coolwarm')
color_zone_A = [cmap(norm(4.))]
color_zone_B = [cmap(norm(3.))]
color_zone_C = [cmap(norm(7.))]
color_zone_D = [cmap(norm(6.))]
color_zone_E = [cmap(norm(1.5))]
color_zone_F = [cmap(norm(0.2))]

mask_central_kpc = 3.

MaxAgeYoungStarsMyr = 1000.
r_img_kpc = 25.
slice_thickness_in_kpc = 10.
npix  = int(4096/4)
vmin  = 5.02
vmax  = 8.48
vmax_sigma_line = 12.
cmap_stars = 'bone'
sigma_min = np.power(10., vmin)
radialbin_kpc = 0.5
pixsize_kpc = r_img_kpc / float(npix)

eta_res = 1.2348
