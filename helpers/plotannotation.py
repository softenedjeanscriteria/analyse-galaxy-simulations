from matplotlib import patches


############################################################################
# Add arrows to connect subplots
############################################################################
def add_arrows(fig,ax):
    # from center to top right
    arrow0 = patches.ConnectionPatch(
        [0.5, 1.0],
        [0., 0.5],
        coordsA=ax[2].transAxes,
        coordsB=ax[1].transAxes,
        color="black",
        arrowstyle="-|>",  # "normal" arrow
        mutation_scale=30,  # controls arrow head size
        linewidth=3,
    )

    # from center to bottom right
    arrow1 = patches.ConnectionPatch(
        [1.0, 0.5],
        [0.5, 1.0],
        coordsA=ax[2].transAxes,
        coordsB=ax[4].transAxes,
        color="black",
        arrowstyle="-|>",  # "normal" arrow
        mutation_scale=30,  # controls arrow head size
        linewidth=3,
    )

    # from center to bottom left
    arrow2 = patches.ConnectionPatch(
        [0.5, 0.0],
        [1.0, 0.5],
        coordsA=ax[2].transAxes,
        coordsB=ax[3].transAxes,
        color="black",
        arrowstyle="-|>",  # "normal" arrow
        mutation_scale=30,  # controls arrow head size
        linewidth=3,
    )

    # from center to top left
    arrow3 = patches.ConnectionPatch(
        [0.0, 0.5],
        [0.5, 0.0],
        coordsA=ax[2].transAxes,
        coordsB=ax[0].transAxes,
        color="black",
        arrowstyle="-|>",  # "normal" arrow
        mutation_scale=30,  # controls arrow head size
        linewidth=3,
    )

    fig.patches.append(arrow0)
    fig.patches.append(arrow1)
    fig.patches.append(arrow2)
    fig.patches.append(arrow3)

    return


