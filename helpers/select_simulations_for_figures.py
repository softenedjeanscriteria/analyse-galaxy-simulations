#################################################
# Ploeckinger et al. (2023, subm)
#################################################

from simulations_details import get_runfolders

#################################################
# Simulations to plot for Figure 8
# 3 different simulations; figure captions assume
# identical values for epsilon, e_sf, and mB and
# different values for h_min
#################################################
Fig8_mB   = ["M5", "M5", "M5"]   
Fig8_eps  = [0.25, 0.25, 0.25]    # kpc
Fig8_hmin = [0.0775, 0.0245, 0.00775]  # kpc
Fig8_sfe  = [0.00316, 0.00316, 0.00316]

Fig8_runnames, Fig8_filebase_runs, Fig8_filebase_reruns, Fig8_filebase_fofruns = get_runfolders(Fig8_mB, Fig8_eps, Fig8_hmin, Fig8_sfe)

#################################################
# Simulations to plot for Figure 9
# 5 different simulations, if different 
# simulations are selected, some annotations might
# have to be manually updated
#################################################

Fig9_mB   = ["M5", "M5", "M5", "M5", "M6"]
Fig9_eps  = [0.25, 0.5, 0.25, 0.25, 0.25]
Fig9_hmin = [0.0775, 0.0775, 0.0775, 0.00775, 0.0775]
Fig9_sfe  = [0.01, 0.00316, 0.00316, 0.00316, 0.00316]

Fig9_runnames, Fig9_filebase_runs, Fig9_filebase_reruns, Fig9_filebase_fofruns = get_runfolders(Fig9_mB, Fig9_eps, Fig9_hmin, Fig9_sfe)

#################################################
# Simulations to plot for Figure 10
# 4 different simulations in total
# 2 for each mass resolution level (10a, 10b)
#################################################

Fig10a_mB   = ["M5", "M5"]
Fig10a_eps  = [0.25, 0.5]
Fig10a_hmin = [0.0775, 0.00775]
Fig10a_sfe  = [0.00316, 0.00316]

Fig10a_runnames, Fig10a_filebase_runs, Fig10a_filebase_reruns, Fig10a_filebase_fofruns = get_runfolders(Fig10a_mB, Fig10a_eps, Fig10a_hmin, Fig10a_sfe)

Fig10b_mB   = ["M6", "M6"]
Fig10b_eps  = [0.25, 0.5]
Fig10b_hmin = [0.0775, 0.00775]
Fig10b_sfe  = [0.00316, 0.00316]

Fig10b_runnames, Fig10b_filebase_runs, Fig10b_filebase_reruns, Fig10b_filebase_fofruns = get_runfolders(Fig10b_mB, Fig10b_eps, Fig10b_hmin, Fig10b_sfe)

#################################################
# Simulations to plot for Figure 11
# 6 different simulations in total
#################################################

Fig11_mB   = ["M5", "M5", "M5", "M5", "M5", "M5"]
Fig11_eps  = [0.25, 0.25, 0.25, 0.50, 0.50, 0.50]
Fig11_hmin = [0.00775, 0.0245, 0.0775, 0.00775, 0.0245, 0.0775]
Fig11_sfe  = [0.00316, 0.00316, 0.00316, 0.00316, 0.00316, 0.00316]

Fig11_runnames, Fig11_filebase_runs, Fig11_filebase_reruns, Fig11_filebase_fofruns = get_runfolders(Fig11_mB, Fig11_eps, Fig11_hmin, Fig11_sfe)



