
#################################################
# Set the path to the simulation directory
#################################################
simulation_path = "/PATH/TO/SIMULATION/FOLDER/setup-galaxy-simulations/"

#################################################
# Select the snapshot number of original simulation
# from which the simulation is restarted
# (default: 100)
#################################################
snapshotnumber = 100
