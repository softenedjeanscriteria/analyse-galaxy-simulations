# Analyse isolated galaxy simulations and reproduce the plots from Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721))

The python routines to reproduce each figure from Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721)) can be found in this repository. 

### Preparation

**Required python packages for all plots:**
- matplotlib
- numpy
- unyt

**Additional python packages required for individual plots:**
- astropy (Fig. 4) 
- swiftsimio (Figs. 8, 9, 10)
- h5py (Figs. 9, 10)
- scipy (Fig. 11)

**Clone the repository:**
```
git clone https://gitlab.phaidra.org/softenedjeanscriteria/analyse-galaxy-simulations.git
```

**Set the path to the isolated galaxy simulations:**

For the figures showing results from the isolated galaxy simulations (Fig. 8, 9, 10, and 11), the simulations have to be run first by following the instructions [here](https://gitlab.phaidra.org/softenedjeanscriteria/setup-galaxy-simulations). 

After all simulations have finished successfully, the path to the simulation folder needs to be set in `helpers/simulation_path.py` by updating this line:

```
simulation_path = "/PATH/TO/SIMULATION/FOLDER/setup-galaxy-simulations/"
```

**Reproduce individual figures:**
The individual figures from Ploeckinger et al. ([arXiv](https://arxiv.org/abs/2310.10721)) are reproduced by running the corresponding python scripts without any additional arguments. For example, Fig. 1 is reproduced with:

```
python3 Fig1_Jeans_mass_comparison.py
```

### Comments for individual figures ###


**Fig. 1: Comparison between Newtonian and softened Jeans mass**

Easily adapt this figure to different constant Plummer-equivalent softening lengths by changing the line `eps = 100. # in pc`.

**Fig. 2: Illustration of the inaccuracy of the SPH-estimated density depending on a minimum smoothing length**

Re-running the figures can lead to slightly different results because the particle positions are random. The minimum smoothing length can be varied by changing the line `hmin_in_pc = 1.55 * 10. # minimum smoothing length in pc`.

**Fig. 3: Runaway collapse zone**

The plotted resolution parameters can be updated by changing the lines that set `epsarr`, `mBarr`, and `h_min_ratio_arr` (see comments in the python script).  

**Fig. 4: Runaway collapse zones examples from the literature**

The astropy python package is used to convert the over-density into a physical density for the Springel & Hernquist (2003) effective equation of state. If the relevant lines that add this line to the subplot are commeted out, the astropy package is not necessary. The parameters for some other simulation projects can be found in the python script and can be used for any of the subplots by modifying the call to `make_subplot`. Some examples are commented out under "other options" towards the end of the python script. 

**Fig. 5: Gravitational stability at the length scale of a smoothing kernel (constant softening)**

Change the plot for different resolution parameters by modifying the lines:
```
mB = [1.e5/8., 1.e5, 8.e5] # msun
mB_default = 1
lsoft = [10., 20., 50., 100., 200., 500.] # pc
lsoft_default = 3
```
Here, `mB` is the baryon particle mass in solar masses, `lsoft` the softening length in pc, and `mB_default` and `lsoft_default` are the indices for the default values for each panel. 

**Fig. 6: Gravitational stability at the length scale of a smoothing kernel (adaptive softening)**

Change the plot for different resolution parameters by modifying the lines:
```
######### Plot nr 1. ########

lsoftmin = 2.25
lsmoothmin = 2.25
mB = 6.4e4 
```
to update the left panel and 
```
######### Plot nr 2. ########
lsoftmin = 108.
lsmoothmin = 0.
mB = 8.5e4
```
to update the right panel. Here, `lsoftmin` is the minimum softening length in pc, `lsmoothmin` the minimum smoothing length in pc, and `mB` the baryon particle mass in solar masses. 

**Fig. 7: Gravitational stability within a smoothing kernel**

Change the plot for different resolution parameters by modifying the lines:
```
    ######### Plot nr 1 ###############
    mB   = 1.e5
    epsarr = [100.]
```
to update the left panel and 
```
    ######### Plot nr 2 ###############
    mB   = 1.e5
    epsarr = [10., 20., 50., 100., 200., 500.]
``` 
to update the right panel. Again, `mB` is the baryon particle mass in solar masses and `epsarr` is the softening length in pc.

**Fig. 8: Density estimate comparison**

By default the same subset of simulations is shown as in the paper but to show a different subset, the following lines in `helpers/select_simulations_for_figures.py` can be updated:
```
Fig8_mB   = ["M5", "M5", "M5"]
Fig8_eps  = [0.25, 0.25, 0.25]    # kpc
Fig8_hmin = [0.0775, 0.0245, 0.00775]  # kpc
Fig8_sfe  = [0.00316, 0.00316, 0.00316]
```

**Fig. 9: Gas and stars gallery**

By default the same subset of simulations is shown as in the paper but to show a different subset, the following lines in `helpers/select_simulations_for_figures.py` can be updated:
```
Fig9_mB   = ["M5", "M5", "M5", "M5", "M6"]
Fig9_eps  = [0.25, 0.5, 0.25, 0.25, 0.25]
Fig9_hmin = [0.0775, 0.0775, 0.0775, 0.00775, 0.0775]
Fig9_sfe  = [0.01, 0.00316, 0.00316, 0.00316, 0.00316]
```
Note, that there are 2 independent scripts for the left (gas, `Fig9a_gallery_gas.py`) and the right half (stars, `Fig9b_gallery_stars.py`) of the figure.

**Fig. 10: Gas and stars alterative parameters**

By default the same subset of simulations is shown as in the paper but to show a different subset, the following lines in `helpers/select_simulations_for_figures.py` can be updated:
```
Fig10a_mB   = ["M5", "M5"]
Fig10a_eps  = [0.25, 0.5]
Fig10a_hmin = [0.0775, 0.00775]
Fig10a_sfe  = [0.00316, 0.00316]
``` 
for the left panel and 
```
Fig10b_mB   = ["M6", "M6"]
Fig10b_eps  = [0.25, 0.5]
Fig10b_hmin = [0.0775, 0.00775]
Fig10b_sfe  = [0.00316, 0.00316]
```
for the right panel. 

**Fig. 11: Star formation rates**

By default the same subset of simulations is shown as in the paper but to show a different subset, the following lines in `helpers/select_simulations_for_figures.py` can be updated:

```
Fig11_mB   = ["M5", "M5", "M5", "M5", "M5", "M5"]
Fig11_eps  = [0.25, 0.25, 0.25, 0.50, 0.50, 0.50]
Fig11_hmin = [0.00775, 0.0245, 0.0775, 0.00775, 0.0245, 0.0775]
Fig11_sfe  = [0.00316, 0.00316, 0.00316, 0.00316, 0.00316, 0.00316]
```


# Authors and acknowledgment
S. Ploeckinger, University of Vienna
